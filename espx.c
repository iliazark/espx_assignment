

#include"covidTrace.h"


int main(){

  if (pthread_mutex_init(&lock, NULL) != 0){
      printf("\n mutex init failed\n");
      return 1;
  }

  gettimeofday(&start, NULL);

  simpleContactsLength=closeContactsLength=0;

  //initialize pointers for use with realloc
  simpleContacts = malloc(0);
  closeContacts = malloc(0);


  //start a thread wich runs every 4 hours for the test
  pthread_create (&threadID[0], NULL, testCOVID, NULL);

  //start a thread wich removes close contacts
  pthread_create (&threadID[2], NULL, checkCloseContacts, NULL);

  //start a thread wich removes simple contacts
  pthread_create (&threadID[3], NULL, checkSimpleContacts, NULL);


  printf("\n");
  unsigned long long previous_time=getUsFromStart();

  //10s loop for the BT search
  while(1){
    //search for nearby bluetooth devices
    macaddress nearbyDeviceMacAddress=BTnearMe();
    // printf("nearbyDeviceMacAddress =%lu\n", (unsigned long)nearbyDeviceMacAddress); //debugging
    if(nearbyDeviceMacAddress!=0){

      //log to bin
      struct binData{
        uint8_t binID;
        unsigned long long timestamp;
        macaddress binMacaddress;
      }data;

      data.binID=0;
      data.timestamp=getUsFromStart();
      data.binMacaddress=nearbyDeviceMacAddress;

      logToBin((uint8_t*)&data,sizeof(data));

      pthread_mutex_lock (&lock);

      int simpleLength = simpleContactsLength;
      for(int i=0; i<=simpleLength; i++){

        if((simpleContactsLength==0)  || i==(simpleContactsLength)){
          // printf("not found in simple con\n");     //for debugging
          //macaddress not found in simple contacts
          int closeLength = closeContactsLength;
          for(int j=0; j<=closeLength; j++){
            // printf("**************6************");
            if((closeContactsLength==0)  || j==(closeContactsLength)){
              // printf("not found in close con\n");    //for debugging
              //macaddress not found in close contacts
              contact newCloseContact;
              newCloseContact.macaddress=nearbyDeviceMacAddress;
              newCloseContact.time=getUsFromStart();
              addSimpleContact(newCloseContact);
              break;
            }else if(nearbyDeviceMacAddress==closeContacts[j].macaddress){
              //macaddress found in close contacts
              // printf("found in close con\n");    //for debugging
              //reset its time
              closeContacts[j].time=getUsFromStart();
              break;
            }
          }
        }else if(nearbyDeviceMacAddress==simpleContacts[i].macaddress){
          // printf("found in simple con\n");    //for debugging
          //macaddress found in simple contacts
          //if max simple contact time has passed, remove from simple and add to close
          if((getUsFromStart()-simpleContacts[i].time) > MIN_CLOSE_CONTACT_TIME){
            // printf("max simple contact time passed\n");    //for debugging
            removeSimpleContact(i);
            contact newCloseContact;
            newCloseContact.macaddress=nearbyDeviceMacAddress;
            newCloseContact.time=getUsFromStart();
            addCloeseContact(newCloseContact);
          }
          break;
        }
      }
    }

    pthread_mutex_unlock (&lock);

    unsigned long long time=getUsFromStart()-previous_time;
    previous_time=getUsFromStart();
    printf("%llu\n",time);
    //sleep until the next BT search
    unsigned long long int sleepTime = TIME_BEWEEN_BT_SEARCHES-(getUsFromStart()%TIME_BEWEEN_BT_SEARCHES);
    usleep(sleepTime);

  }
}

macaddress BTnearMe(){
  unsigned long long currentTime=getUsFromStart();
  unsigned long long currentSecond=currentTime/10000;
  unsigned long long currentHour=currentSecond/3600;
  unsigned long long currentDay=currentHour/24;
  unsigned long long secondOfDay=currentSecond%86400;
  unsigned long long hourOfDay=currentHour%24;
  unsigned long long dayOfTheWeek=(currentDay%7)+1;

  //spends time with his wife most afternoons
  // printf("h=%llu  ",currentHour);
  if(hourOfDay>10 && dayOfTheWeek<6){
    return 0x6F5AC7169F1E;
  }

  //coworker
  if(hourOfDay>2 && dayOfTheWeek<6){
    return 0x6C5AA3149F11;
  }

  //boss meeting
  if(hourOfDay>1 && dayOfTheWeek<6){
    return 0x9C7BA3149A13;
  }

  //meets people on his way to work
  if(secondOfDay>7100 && dayOfTheWeek==2){
    return 0x9A7BA9249A43;
  }
  if(secondOfDay>7100 && dayOfTheWeek==5){
    return 0x971BA9D39A4F;
  }

  //meets his friend on weekend afternoons
  if(hourOfDay>10 && hourOfDay<17 && dayOfTheWeek>5){
    return 0xAF8ACB489F16;
  }

  //meets his mother on weekend mornings (his dad passed away a couple years ago)
  if(hourOfDay>3 && hourOfDay<8 && dayOfTheWeek>5){
    return 0x49FACB4D9217;
  }

  //else he spends dome time alone
  return 0;
}

void* testCOVID(){
  while(1){
    unsigned long long currentDay=getUsFromStart()/864000000;

    pthread_mutex_lock (&lock);

    struct binData{
    uint8_t binID;
    unsigned long long timestamp;
    uint8_t results;
    }data;

    data.binID=1;
    data.timestamp=getUsFromStart();

    if(currentDay==13){
      uploadContacts(closeContacts, closeContactsLength);
      data.results=1;
    }else{
      data.results=0;
    }

    pthread_mutex_unlock (&lock);

    //log to binary
    logToBin((uint8_t*)&data,sizeof(data));

    unsigned long long int sleepTime = TIME_BETWEEN_TESTS-(getUsFromStart()%TIME_BETWEEN_TESTS);
    // printf("1 %llu\n",sleepTime); // for debugging
    usleep(sleepTime);
  }
}

void uploadContacts(contact *list, int length){
  //write to bin file
  for(int i=0; i< length; i++){
    struct binData{
    uint8_t binID;
    unsigned long long timestamp;
    contact binContact;
    }data;

    data.binID=2;
    data.timestamp=getUsFromStart();
    data.binContact=list[i];

    logToBin((uint8_t*)&data,sizeof(data));
  }

}

void* checkCloseContacts(){
  while(1){
    unsigned long long int oldestContactTime=getUsFromStart();
    pthread_mutex_lock (&lock);

    for(int j=0; j<closeContactsLength; j++){

      //remove if enough ime has passed
      if((getUsFromStart()-closeContacts[j].time)>TIME_TO_FORGET_CLOSE_CONTACTS){
        removeCloeseContact(j);
      }else if(closeContacts[j].time<oldestContactTime){
        //find the eldest contact
        oldestContactTime=closeContacts[j].time;
      }
    }
    pthread_mutex_unlock (&lock);

    //find the maximum amount of sleeping time
    if((getUsFromStart()-oldestContactTime)<TIME_TO_FORGET_CLOSE_CONTACTS){
      unsigned long long int sleepTime = TIME_TO_FORGET_CLOSE_CONTACTS-(getUsFromStart()-oldestContactTime);
      usleep(sleepTime);
    }else{
      usleep(TIME_TO_FORGET_CLOSE_CONTACTS);
    }
  }
}

void* checkSimpleContacts(){
  while(1){
    unsigned long long int oldestContactTime=getUsFromStart();
    pthread_mutex_lock (&lock);

    for(int j=0; j<simpleContactsLength; j++){

      //remove if enough ime has passed
      if((getUsFromStart()-simpleContacts[j].time)>TIME_TO_FORGET_SIMPLE_CONTACTS){
        removeSimpleContact(j);
      }else if(simpleContacts[j].time<oldestContactTime){
        //find the eldest contact
        oldestContactTime=simpleContacts[j].time;
      }
    }
    pthread_mutex_unlock (&lock);
    // printf("oldestContactTime = %llu  getUsFromStart() = %llu \n", (unsigned long long)oldestContactTime,(unsigned long long)getUsFromStart());

    //find the maximum amount of sleeping time
    if((getUsFromStart()-oldestContactTime)<TIME_TO_FORGET_SIMPLE_CONTACTS){
      unsigned long long int sleepTime = TIME_TO_FORGET_SIMPLE_CONTACTS-(getUsFromStart()-oldestContactTime);
      usleep(sleepTime);
    }else{
      usleep(TIME_TO_FORGET_SIMPLE_CONTACTS);
    }
  }
}

void addSimpleContact(contact newContact){
  // pthread_mutex_lock (&lock);
  simpleContacts=realloc(simpleContacts,(++simpleContactsLength)*(sizeof(contact)));
  simpleContacts[simpleContactsLength-1]=newContact;
  // pthread_mutex_unlock (&lock);
  printf("new simple contact with macaddress: %lu\n", (unsigned long)newContact.macaddress);
}

void removeSimpleContact(int contactNumber){
  printf("removed simple contact with macaddress: %lu\n", (unsigned long)simpleContacts[contactNumber].macaddress);
  simpleContacts[contactNumber]=simpleContacts[simpleContactsLength-1];
  simpleContacts=realloc(simpleContacts,(--simpleContactsLength)*(sizeof(contact)));
}

void addCloeseContact(contact newContact){
  // pthread_mutex_lock (&lock);
  closeContacts=realloc(closeContacts,(++closeContactsLength)*(sizeof(contact)));
  closeContacts[closeContactsLength-1]=newContact;
  // pthread_mutex_unlock (&lock);
  printf("new close contact with macaddress: %lu\n", (unsigned long)newContact.macaddress);
}

void removeCloeseContact(int contactNumber){
  printf("removed close contact with macaddress: %lu\n", (unsigned long)simpleContacts[contactNumber].macaddress);
  closeContacts[contactNumber]=closeContacts[closeContactsLength-1];
  closeContacts=realloc(closeContacts,(--closeContactsLength)*(sizeof(contact)));
}

unsigned long long int getUsFromStart(){
  struct timeval stop;
  gettimeofday(&stop, NULL);
  return (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec;
}

void logToBin(uint8_t *data, int length){

  fptr = fopen("log.bin","ab");
  fwrite(data, 1, length, fptr);
  fclose(fptr);

}
