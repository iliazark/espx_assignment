#ifndef COVID_TRACE
#define COVID_TRACE

#define _DEFAULT_SOURCE

#include<pthread.h>
#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<unistd.h>
#include<sys/time.h>
#include<stdbool.h>
#include<time.h>

FILE *fptr;

struct timeval start;

pthread_mutex_t lock;
pthread_t threadID[4];

typedef uint64_t macaddress;

typedef struct {
  macaddress macaddress;
  unsigned long long int time;
} contact;

unsigned long long int TIME_BEWEEN_BT_SEARCHES         =100000;      //in us (10s/100)
unsigned long long int TIME_BETWEEN_TESTS              =144000000;   //in us (4hours/100)
unsigned long long int MIN_CLOSE_CONTACT_TIME          =2400000;     //in us (4mins/100)
unsigned long long int MAX_CLOSE_CONTACT_TIME          =12000000;    //in us (20mins/100)
unsigned long long int TIME_TO_FORGET_CLOSE_CONTACTS   =12096000000; //in us (14days/100)
unsigned long long int TIME_TO_FORGET_SIMPLE_CONTACTS  =12000000;    //in us (20mins/100)

contact *simpleContacts, *closeContacts;
int simpleContactsLength, closeContactsLength;

void uploadContacts(contact *list, int length);
void* testCOVID();
macaddress BTnearMe();
void* checkCloseContacts();
void* checkSimpleContacts();
void addSimpleContact(contact newContact);
void removeSimpleContact(int contantNumber);
void addCloeseContact(contact newContact);
void removeCloeseContact(int contantNumber);
unsigned long long int getUsFromStart();
void logToBin(uint8_t *data, int length);

#endif
